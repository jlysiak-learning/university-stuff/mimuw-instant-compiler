#!/bin/bash

TEST_DIR=tests
TMP=${TEST_DIR}/test-output

function run_tests {
        COMPILER=$1
        for el in $(ls -1 ${TEST_DIR}/*.ins);
        do
                fullname=$(basename ${el})
                basename=$(echo ${fullname} | cut -d'.' -f 1)
                cmp=${TEST_DIR}/${basename}.output

                ./${COMPILER} ${el}

                if [[ ${COMPILER} == insc_jvm ]]; then
                        java -cp ${TEST_DIR} ${basename} > ${TMP}
                else
                        llvm_exec=${TEST_DIR}/${basename}.bc
                        lli ${llvm_exec} > ${TMP}
                fi

		diff $TMP $cmp &> /dev/null
                if [ $? -gt 0 ]; then
                        echo -e "[\033[91mFAIL\033[0m]: \033[93m$el\033[0m"
                        diff $TMP $cmp
                else
                        echo -e "[ \033[92mOK\033[0m ]: \033[93m$el\033[0m"
                fi
                rm -f $TMP ${TEST_DIR}/*.j ${TEST_DIR}/*.class ${TEST_DIR}/*.ll ${TEST_DIR}/*.bc
        done

}

case $1 in
        jvm)
                echo "Running tests for JVM..."
                run_tests insc_jvm
                echo
                echo DONE!
                ;;
        llvm)
                echo "Running tests for LLVM..."
                run_tests insc_llvm
                echo
                echo DONE!
                ;;
        *)
                echo "Usage: ./test.sh {jvm|llvm}"
                echo "Bye!"
                ;;
esac


