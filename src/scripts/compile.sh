#!/bin/bash

# Compiler glue!

# USAGE: compile.sh ./{insc_jvm|insc_llvm} INFILE

BINARY_DIR=out
BIN=$(basename ${1})
BINARY="./${BINARY_DIR}/${BIN}"

IN_FULL=${2}
IN_DIR=$(dirname ${IN_FULL})
FULLNAME=$(basename ${IN_FULL})
BASENAME=$(echo ${FULLNAME} | cut -d'.' -f 1)
OUT_BASE=${IN_DIR}/${BASENAME}

LIB_LLVM_RUNTIME_LL=lib/runtime.ll
LIB_LLVM_RUNTIME_BC=out/runtime.bc
JASMIN=lib/jasmin.jar

case ${BIN} in
        insc_jvm)
                ${BINARY} ${IN_FULL} ${BASENAME} > ${OUT_BASE}.j
                if [[ "$?" != 0 ]]; then
                        rm ${OUT_BASE}.j
                        exit 1
                fi
                java -jar ${JASMIN} -d ${IN_DIR} ${OUT_BASE}.j
                ;;

        insc_llvm)
                ${BINARY} ${IN_FULL} > ${OUT_BASE}.ll
                llvm-as -o ${OUT_BASE}_.bc ${OUT_BASE}.ll
                llvm-as -o ${LIB_LLVM_RUNTIME_BC} ${LIB_LLVM_RUNTIME_LL}
                llvm-link -o ${OUT_BASE}.bc ${LIB_LLVM_RUNTIME_BC} ${OUT_BASE}_.bc
                #rm ${OUT_BASE}_.bc ${LIB_LLVM_RUNTIME_BC}
                ;;
        *)
                echo Wrong binary!
                exit
                ;;
esac




