#!/usr/bin/env python3
import os
import argparse
import subprocess
from random import choice


def generate(expr, depth, var):

    vallist = list(range(1, 10))
    tlist = ['+', '-', '*', '/']
    variables = ['a%d' % i for i in range(var)]
    variables_d = dict([(x, choice(vallist)) for x in variables])

    def gexpr(d):
        if d is 0:
            if choice([0,1]) or var == 0:
                v = choice(vallist)
            else:
                v = choice(variables)
            return {'type':'n', 'val':v}, str(v)
        e1, s1 = gexpr(d-1)
        e2, s2 = gexpr(d-1)
        ops = choice(tlist)
        if ops in ['*', '/'] and (e1['type'] in ['+', '-']):
            s = '(' + s1 + ')' + ops
        else:
            s = s1 + ops
        if ops in ['*', '/'] and e2['type'] in ['+', '-']:
            s += '(' + s2 + ')'
        else:
            s += s2
        return {'type':ops, 'l':e1, 'r':e2}, s
    lines = ["%s=%d;\n"%(k,v) for k,v in variables_d.items()]
    content = ' '.join(lines) 
    exps = [gexpr(depth)[1] for _ in range(expr)]
    return content + ';\n'.join(exps) + ';\n'


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--expr', type=int, help='Expressions number', default=1)
    parser.add_argument('-d', '--depth', type=int, help='Expression depth', default=0)
    parser.add_argument('-v', '--var', type=int, help='Variables available', default=0)
    parser.add_argument('-t', '--tests', type=int, help='Tests', default=10)
    parser.add_argument('-s', '--start', type=int, help='Tests start number', default=0)
    parser.add_argument('--dir', default='.')
    args = parser.parse_args()

    os.chdir(args.dir)    
    i = args.start
    while i < args.start+args.tests:
        print('======', i)
        exps = generate(args.expr, args.depth, args.var)
        with open('test%d.ins' % i, 'w') as f:
            f.write(exps)
        cat = subprocess.Popen(('cat', 'test%d.ins' % i), stdout=subprocess.PIPE)
        bc = subprocess.check_output(['bc','-q'], stdin=cat.stdout, stderr=subprocess.STDOUT).decode('ascii')
        if bc.find('error') >= 0:
            continue
        with open('test%d.output' %i, 'w') as f:
            f.write(bc)
        i = i + 1



