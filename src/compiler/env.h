/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * Environment methods.
 */

#ifndef _ENV_H_
#define _ENV_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct env env_t;

env_t * env_create_loc_store();
void env_destroy_loc_store(env_t *env);
int env_get_ident_loc(env_t * env, const char * ident);
int env_alloc_ident_loc(env_t * env, const char * ident);
int env_get_locals_used(env_t * env);

#ifdef __cplusplus
}
#endif

#endif
