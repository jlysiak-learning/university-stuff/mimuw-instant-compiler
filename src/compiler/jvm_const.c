/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * JVM strings.
 */

const char jvm_const_invocation_fmt[] = "\
.class public %s \n\
.super java/lang/Object \n\
\n\
.method public <init>()V\n\
\taload_0\n\
\tinvokespecial java/lang/Object/<init>()V\n\
\treturn\n\
.end method\n\
\n";

const char jvm_const_main[] ="\
.method public static main([Ljava/lang/String;)V\n";

const char jvm_const_fun[] ="\
.method public static fun()V\n";

const char jvm_const_limit_stack_fmt[] = ".limit stack %d\n";
const char jvm_const_limit_locals_fmt[] = ".limit locals %d\n";

const char jvm_const_swap[] = "swap\n";
const char jvm_const_getstatic_printstream[] = "getstatic java/lang/System/out Ljava/io/PrintStream;\n";
const char jvm_const_invokevirtual_println[] = "invokevirtual java/io/PrintStream/println(I)V\n";

const char jvm_const_iconst_fmt[] = "iconst_%d\n";
const char jvm_const_sipush_fmt[] = "sipush %d\n";
const char jvm_const_bipush_fmt[] = "bipush %d\n";
const char jvm_const_ldc_fmt[] = "ldc %d\n";

const char jvm_const_istore_fmt[] = "istore %d\n";
const char jvm_const_istore__fmt[] = "istore_%d\n"; // 0, 1, 2, 3

const char jvm_const_iload_fmt[] = "iload %d\n";
const char jvm_const_iload__fmt[] = "iload_%d\n"; // 0, 1, 2, 3

const char jvm_const_iadd[] = "iadd\n";
const char jvm_const_isub[] = "isub\n";
const char jvm_const_imul[] = "imul\n";
const char jvm_const_idiv[] = "idiv\n";

const char jvm_const_return[] = "return\n";
const char jvm_const_end_method[] = ".end method\n";

const char jvm_const_invokestatic_fun_fmt[] = "invokestatic %s/fun()V\n";
