/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Code blocks
 */

#ifndef __BLOCKS__
#define __BLOCKS__

#include <stdio.h>
#include "Absyn.h"

#define JVM     0
#define LLVM    1

/** Single code block structure */
typedef struct expr_block {
    struct expr_block *fst; // First operant expression
    struct expr_block *snd; // Second operand expression
    char *content;          // Expression op. instruction code

#if TARGET == JVM
    int stack_limit;        // Stact size required to calculate expression
    int use_swap;           // Use swap instruction when generating expr. code 
#elif TARGET == LLVM
    int id;                 // Expression node ID (preorder)
    int size;               // Expression tree size
    char *ref;              // String reference to register containg expr. value
#endif
} expr_block_t;

/** Single statement code block 
 *  structure */
typedef struct stmt_block {
    struct stmt_block * next;   // Next statement block
    expr_block_t * expr;        // Expression evaluated in statement
    int assign;                 // Variable location, -1 if none
#if TARGET == JVM
    int stack_limit;            // Stack limit for evaluated expression 
#elif TARGET == LLVM
    int next_id;                // Next available ID
#endif
} stmt_block_t;

/** Program code block structure */
typedef struct prog_block {
    stmt_block_t * stmts;       // List of statements
    int locals;                 // Number local of variables used
#if TARGET == JVM
    int stack_limit;            // Stack required to run this program
#elif TARGET == LLVM

#endif
} prog_block_t;


prog_block_t * prog_block_create();
void prog_block_destroy(prog_block_t * block);
void prog_block_set_locals_limit(prog_block_t * block, int limit);

#if TARGET == JVM
void prog_block_print(prog_block_t * block, FILE * output, const char * name);
void prog_block_set_stack_limit(prog_block_t * block);
#elif TARGET == LLVM
void prog_block_print(prog_block_t * block, FILE * output);
#endif

stmt_block_t * stmt_block_create();
void stmt_block_destroy(stmt_block_t * block);
void stmt_block_build_assign(stmt_block_t * block, int loc, expr_block_t *expr);
void stmt_block_build_expr(stmt_block_t * block, expr_block_t *expr);
void stmt_block_print(stmt_block_t * block, FILE * output);

expr_block_t * expr_block_create();
void expr_block_destroy(expr_block_t * block);
void expr_block_const(expr_block_t * block, int val);
void expr_block_load(expr_block_t * block, int val);
void expr_block_print(expr_block_t * block, FILE * output);

#if TARGET == JVM
void expr_block_build_op(expr_block_t * block, expr_block_t * block_a, expr_block_t * block_b, int opt);
void expr_block_op(Exp _p_, expr_block_t * block);
#elif TARGET == LLVM
void expr_block_op(Exp _p_, expr_block_t * block, expr_block_t * block_a, expr_block_t * block_b);
#endif

#endif // __BLOCKS__
