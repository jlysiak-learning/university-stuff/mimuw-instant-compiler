/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * LLVM backend.
 */

#ifndef __LLVM_COMPILER__
#define __LLVM_COMPILER__

#include <stdio.h>

void llvm_compiler_run(Program p, FILE *output);

#endif // __LLVM_COMPILER__
