/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant copiler
 *
 * Compilation result is printed on STDOUT.
 * Use redirection operator to save into file.
 *
 * Usage: ./<jvm binary> <Instant source file> <class name>
 * Usage: ./<llvm binary> <Instant source file>
 */

#include <stdio.h>
#include <stdlib.h>

#include "Parser.h"
#include "Absyn.h"


#ifndef TARGET
#error "Target was not defined! Use -DTARGET=0 or 1"
#endif

#define JVM     0
#define LLVM    1

#if TARGET == JVM

#include "jvm.h"
#define ARGS 2

#elif TARGET == LLVM    

#include "llvm.h"
#define ARGS 1

#endif


int main(int argc, char ** argv)
{
    FILE *input;
    FILE *output;

    Program parse_tree;
    if (argc > ARGS) {
        input = fopen(argv[1], "r");
        if (!input) {
            fprintf(stderr, "Error opening input file.\n");
            exit(1);
        }
    } else {
        fprintf(stderr, "Error: Input file and class name not specified.\n");
        exit(1);
    }

    parse_tree = pProgram(input);
    if (!parse_tree) 
        return 1;

    /* Print results on STDOUT */
    output = stdout;

#if TARGET == JVM
    jvm_compiler_run(parse_tree, output, argv[2]);

#elif TARGET == LLVM    
    llvm_compiler_run(parse_tree, output);

#endif
    return 0;
}

