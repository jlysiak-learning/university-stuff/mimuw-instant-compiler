/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * Main JVM module.
 */

#include <stdio.h>

#include "blocks.h"
#include "env.h"

static prog_block_t * visitProgram(Program p, env_t * env);
static stmt_block_t * visitStmt(Stmt p, env_t * env);
static stmt_block_t * visitListStmt(ListStmt p, env_t * env);
static expr_block_t * visitExp(Exp p, env_t * env);

static char * visitIdent(Ident i);
static int visitInteger(Integer i);

//===================== PRIVATE

static prog_block_t * visitProgram(Program _p_, env_t * env)
{
    prog_block_t * block;
    block = prog_block_create();

    switch (_p_->kind) {
        case is_Prog:
            // Check if not NULL to allow empty programs
            if (_p_->u.prog_.liststmt_) {
                block->stmts = visitListStmt(_p_->u.prog_.liststmt_, env);
                if (!block->stmts)
                    goto err;
            }
            prog_block_set_stack_limit(block);
            prog_block_set_locals_limit(block, env_get_locals_used(env));
            break;

        default:
            fprintf(stderr, "Error: bad kind field when analyzing Program!\n");
            goto err;
    }
    return block;

err:
    prog_block_destroy(block);
    return NULL;
}

static stmt_block_t * visitStmt(Stmt _p_, env_t * env)
{
    char * ident;
	int loc;
    stmt_block_t * block;
    expr_block_t * expr_block;

    block = stmt_block_create();

    switch (_p_->kind) {
        case is_SAss:
            ident = visitIdent(_p_->u.sass_.ident_);
            loc = env_alloc_ident_loc(env, ident);
            expr_block = visitExp(_p_->u.sass_.exp_, env);
            if (!expr_block)
                goto err;
            stmt_block_build_assign(block, loc, expr_block);
            break;  

        case is_SExp:
            expr_block = visitExp(_p_->u.sexp_.exp_, env);
            if (!expr_block)
                goto err;
            stmt_block_build_expr(block, expr_block);
            break;

        default:
            fprintf(stderr, "Error: Bad statement field!\n");
            goto err;
    }
    return block;
err:
    stmt_block_destroy(block);
    return NULL;
}

static stmt_block_t * visitListStmt(ListStmt liststmt, env_t * env)
{
    stmt_block_t * block;
    stmt_block_t * head;
    stmt_block_t * block_prev;

    head = NULL;
    block_prev = NULL;
    while (liststmt != 0) {
        block = visitStmt(liststmt->stmt_, env);
        if (!block)
            goto err;
        if (!head)
            head = block;
        block->next = NULL;
        if (block_prev != NULL)
            block_prev->next = block;
        block_prev = block;
        liststmt = liststmt->liststmt_;
    }
    return head;
err:
    stmt_block_destroy(head);
    return NULL;
}

static expr_block_t * visitExp(Exp _p_, env_t *env)
{
    int val;
    char * ident;
    expr_block_t *block_a;
    expr_block_t *block_b;
    expr_block_t *block_result;
    
    block_result = expr_block_create();

    switch (_p_->kind) {
        case is_ExpAdd:
            block_a = visitExp(_p_->u.expadd_.exp_1, env);
            if (!block_a)
                goto err_a;
            block_b = visitExp(_p_->u.expadd_.exp_2, env);
            if (!block_b)
                goto err_b;
            expr_block_build_op(block_result, block_a, block_b, 1);
            expr_block_op(_p_, block_result);
            break;

        case is_ExpSub:
            block_a = visitExp(_p_->u.expsub_.exp_1, env);
            if (!block_a)
                goto err_a;
            block_b = visitExp(_p_->u.expsub_.exp_2, env);
            if (!block_b)
                goto err_b;
            expr_block_build_op(block_result, block_a, block_b, 0);
            expr_block_op(_p_, block_result);
            break;

        case is_ExpMul:
            block_a = visitExp(_p_->u.expmul_.exp_1, env);
            if (!block_a)
                goto err_a;
            block_b = visitExp(_p_->u.expmul_.exp_2, env);
            if (!block_b)
                goto err_b;
            expr_block_build_op(block_result, block_a, block_b, 1);
            expr_block_op(_p_, block_result);
            break;

        case is_ExpDiv:
            block_a = visitExp(_p_->u.expdiv_.exp_1, env);
            if (!block_a)
                goto err_a;
            block_b = visitExp(_p_->u.expdiv_.exp_2, env);
            if (!block_b)
                goto err_b;
            expr_block_build_op(block_result, block_a, block_b, 0);
            expr_block_op(_p_, block_result);
            break;

        case is_ExpLit:
            val = visitInteger(_p_->u.explit_.integer_);
            expr_block_const(block_result, val);
            break;

        case is_ExpVar:
            ident = visitIdent(_p_->u.expvar_.ident_);
            val = env_get_ident_loc(env, ident);
            if (val < 0) {
                fprintf(stderr, "Error: Referencing variable %s before assignment!\n", ident);
                goto err_a;
            }
            expr_block_load(block_result, val);
            break;

        default:
            fprintf(stderr, "Error: bad kind field when visiting Exp!\n");
            goto err_a;
    }
    return block_result;

err_b:
    expr_block_destroy(block_a);
err_a:
    expr_block_destroy(block_result);
    return NULL;
}

static char * visitIdent(Ident i)
{
    return (char *) i;
}

static int visitInteger(Integer i)
{
    return (int) i;
}

//===================== PUBLIC

void jvm_compiler_run(Program p, FILE *output, const char *name)
{
    env_t *env;
    prog_block_t * block;

    env = env_create_loc_store();
    block = visitProgram(p, env);
    if (!block)
        goto err;
    prog_block_print(block, output, name);
    prog_block_destroy(block);
err:
    env_destroy_loc_store(env);
}

