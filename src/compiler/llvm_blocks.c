/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * LLVM code blocks implementation
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "blocks.h"
#include "llvm_const.h"

#define print_tab(OUT) fprintf(OUT, "\t")

//=================== PROGRAM BLOCKS

prog_block_t * prog_block_create()
{
    prog_block_t *b;
    b = (prog_block_t *) malloc(sizeof(prog_block_t));
    b->stmts = NULL;
    b->locals = 0;
    return b;
}

void prog_block_destroy(prog_block_t * block)
{
    if (!block)
        return;
    stmt_block_destroy(block->stmts);
    free(block);
}

void prog_block_set_locals_limit(prog_block_t * block, int limit)
{
    block->locals = limit; // Just number of used variables
}

void prog_block_print(prog_block_t * block, FILE * output)
{
    int i;
    stmt_block_t *stmt;

    stmt = block->stmts;
    
    fprintf(output, "%s", llvm_const_runtime_decl);
    fprintf(output, "%s", llvm_const_main_begin);

    for (i = 0; i < block->locals; i++) { // Allocate memory for variables
        print_tab(output);
        fprintf(output, llvm_const_alloc_fmt, i);
    }

    while (stmt) { // Print statements
        stmt_block_print(stmt, output);
        stmt = stmt->next;
    }
    fprintf(output, "%s", llvm_const_main_end); // Print ret and `}`
}

//=================== STATEMENTS BLOCKS
//
stmt_block_t * stmt_block_create()
{
    stmt_block_t *b;
    b = (stmt_block_t *) malloc(sizeof(stmt_block_t));
    b->next = NULL;
    b->expr = NULL;
    b->assign = -1; // Pure expression
    b->next_id = 0; // Next expression id.
    return b;
}

void stmt_block_destroy(stmt_block_t * head)
{
    if (!head)
        return;
    expr_block_destroy(head->expr);
    stmt_block_destroy(head->next);
    free(head);
}

void stmt_block_build_assign(stmt_block_t * block, int loc, expr_block_t *expr)
{
    block->expr = expr;
    block->assign = loc;
}

void stmt_block_build_expr(stmt_block_t * block, expr_block_t *expr)
{
    block->expr = expr;
}

void stmt_block_print(stmt_block_t * block, FILE *output)
{
    if (block->assign == -1) {
        expr_block_print(block->expr, output); 
        print_tab(output);
        fprintf(output, llvm_const_print_fmt, block->expr->ref);
    } else {
        expr_block_print(block->expr, output); 
        print_tab(output);
        fprintf(output, llvm_const_store_fmt, block->expr->ref, block->assign);
    }
}

//=================== EXPRESSION BLOCKS

expr_block_t * expr_block_create()
{
    expr_block_t *b;
    b = (expr_block_t *) malloc(sizeof(expr_block_t));
    b->fst = NULL;
    b->snd = NULL;
    b->content = NULL;
    b->ref = NULL;
    b->id = 0;
    b->size = 0;
    return b;
}

void expr_block_destroy(expr_block_t * block)
{
    if (!block)
        return;
    expr_block_destroy(block->fst);
    expr_block_destroy(block->snd);
    if (block->content)
        free(block->content);
    if (block->ref)
        free(block->ref);
    free(block);
}

void expr_block_const(expr_block_t * block, int val)
{
    block->fst = NULL;
    block->snd = NULL;
    block->content = NULL;
    block->ref = (char *) malloc(sizeof(char) * 64);
    sprintf(block->ref, "%d", val);
}

void expr_block_load(expr_block_t * block, int loc)
{
    block->fst = NULL;
    block->snd = NULL;
    block->content = (char *) malloc(sizeof(char) * 128);
    block->ref = (char *) malloc(sizeof(char) * 64);
    sprintf(block->ref, "%%e_%d", block->id);
    sprintf(block->content, llvm_const_load_fmt, block->ref, loc);
}

void expr_block_op(Exp _p_, expr_block_t *block, expr_block_t * block_a, expr_block_t * block_b)
{
    const char *const_str;
    switch(_p_->kind)
    {
        case is_ExpAdd:
            const_str = llvm_const_add_fmt;
            break;
        case is_ExpSub:
            const_str = llvm_const_sub_fmt;
            break;
        case is_ExpMul:
            const_str = llvm_const_mul_fmt;
            break;
        case is_ExpDiv:
            const_str = llvm_const_div_fmt;
            break;
        default:
            fprintf(stderr, "Wrong kind of expression in expr_block_op!\n");
            exit(0);
    }
    block->fst = block_a;
    block->snd = block_b;

    block->content = (char *) malloc(sizeof(char) * 128);
    block->ref = (char *) malloc(sizeof(char) * 64);

    sprintf(block->ref, "%%e_%d", block->id);
    sprintf(block->content, const_str, block->ref, block_a->ref, block_b->ref);
}

void expr_block_print(expr_block_t * block, FILE *output)
{
    if (block->fst)
        expr_block_print(block->fst, output); 
    if (block->snd)
        expr_block_print(block->snd, output); 
    if (block->content){
        print_tab(output);
        fprintf(output, "%s", block->content);
    }
}

