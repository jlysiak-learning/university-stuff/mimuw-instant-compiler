/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant copiler
 * JVM strings
 */

#ifndef __JVM_CONST__
#define __JVM_CONST__

extern const char jvm_const_invocation_fmt[];
extern const char jvm_const_main[];
extern const char jvm_const_fun[];
extern const char jvm_const_invokestatic_fun_fmt[];

extern const char jvm_const_limit_stack_fmt[];
extern const char jvm_const_limit_locals_fmt[];
extern const char jvm_const_swap[];
extern const char jvm_const_getstatic_printstream[];
extern const char jvm_const_invokevirtual_println[];

extern const char jvm_const_iconst_fmt[];
extern const char jvm_const_sipush_fmt[];
extern const char jvm_const_bipush_fmt[];
extern const char jvm_const_ldc_fmt[];

extern const char jvm_const_istore_fmt[];
extern const char jvm_const_istore__fmt[]; // 0, 1, 2, 3

extern const char jvm_const_iload_fmt[];
extern const char jvm_const_iload__fmt[]; // 0, 1, 2, 3

extern const char jvm_const_iadd[];
extern const char jvm_const_isub[];
extern const char jvm_const_imul[];
extern const char jvm_const_idiv[];

extern const char jvm_const_return[];
extern const char jvm_const_end_method[];

#endif // __JVM_CONST__
