/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * LLVM strings.
 */

const char llvm_const_runtime_decl[] = "\
declare void @printInt(i32) ;  from lib/runtime.ll\n";
const char llvm_const_main_begin[] = "define i32 @main() {\n";
const char llvm_const_main_end[] = "\tret i32 0\n}\n";

const char llvm_const_alloc_fmt[] = "%%loc_%d = alloca i32\n";
const char llvm_const_load_fmt[] = "%s = load i32, i32* %%loc_%d\n";
const char llvm_const_store_fmt[] = "store i32 %s, i32* %%loc_%d\n";

const char llvm_const_add_fmt[] = "%s = add i32 %s, %s\n";
const char llvm_const_sub_fmt[] = "%s = sub i32 %s, %s\n";
const char llvm_const_mul_fmt[] = "%s = mul i32 %s, %s\n";
const char llvm_const_div_fmt[] = "%s = sdiv i32 %s, %s\n";

const char llvm_const_print_fmt[] = "call void @printInt(i32 %s)\n";

