/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * JVM backend.
 */

#ifndef __JVM_COMPILER__
#define __JVM_COMPILER__

#include <stdio.h>
#include "Absyn.h"

void jvm_compiler_run(Program p, FILE * output, const char * name);

#endif // __JVM_COMPILER__
