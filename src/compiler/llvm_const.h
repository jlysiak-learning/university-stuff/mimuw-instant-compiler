/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * Instant compiler.
 * LLVM strings.
 */

#ifndef __LLVM_CONST__
#define __LLVM_CONST__

extern const char llvm_const_runtime_decl[];
extern const char llvm_const_main_begin[];
extern const char llvm_const_main_end[];

extern const char llvm_const_alloc_fmt[];
extern const char llvm_const_load_fmt[];
extern const char llvm_const_store_fmt[];

extern const char llvm_const_add_fmt[];
extern const char llvm_const_sub_fmt[];
extern const char llvm_const_mul_fmt[];
extern const char llvm_const_div_fmt[];

extern const char llvm_const_print_fmt[];

#endif // __LLVM_CONST__
