#include "env.h"

#include <stdio.h>
#include <string>
#include <map>

struct env {};

class Env : public env {
public:
    Env();
    ~Env();

    int get_ident(std::string ident);
    int set_ident(std::string ident, int val);

    int get_ident_loc(std::string ident);
    int alloc_ident_loc(std::string ident);
    int allocated_locs();

private:
    std::map<std::string, int> dict;
};

inline Env * _real(env_t *env) { return static_cast<Env *>(env); }

//===== ENV IMPLEMENTATION

Env::Env() {}

Env::~Env() {}

int Env::get_ident_loc(std::string ident)
{
    std::map<std::string, int>::iterator it;
    it = dict.find(ident);
    if (it == dict.end())
        return -1; // No such element
    return it->second; // Get element location
}

int Env::alloc_ident_loc(std::string ident)
{
    int loc = get_ident_loc(ident);
    if (loc < 0)
        loc = (int) dict.size();
    dict.insert(std::pair<std::string, int>(ident, loc));
    return loc;
}

int Env::allocated_locs()
{
    return (int) dict.size();
}

//=== JVM ENV INTERFACE

env_t * env_create_loc_store() 
{
    return new Env();
}

void env_destroy_loc_store(env_t * env)
{
    delete _real(env);
}

int env_get_ident_loc(env_t * env, const char * ident)
{
    std::string _ident(ident);
    return _real(env)->get_ident_loc(_ident);
}

int env_alloc_ident_loc(env_t * env, const char * ident)
{
    std::string _ident(ident);
    return _real(env)->alloc_ident_loc(_ident);
}

int env_get_locals_used(env_t * env)
{
    return _real(env)->allocated_locs();
}

