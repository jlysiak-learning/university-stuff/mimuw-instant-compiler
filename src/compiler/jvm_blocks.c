/*
 * Jacek Łysiak <jl345639@students.mimuw.edu.pl>
 *
 * JVM code blocks implementation
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "blocks.h"
#include "jvm_const.h"

#define print_tab(OUT) fprintf(OUT, "\t")

//=================== PROGRAM BLOCKS

prog_block_t * prog_block_create()
{
    prog_block_t *b;
    b = (prog_block_t *) malloc(sizeof(prog_block_t));
    b->stmts = NULL;
    b->locals = 0;
    b->stack_limit = 0;
    return b;
}

void prog_block_destroy(prog_block_t * block)
{
    if (!block)
        return;
    stmt_block_destroy(block->stmts);
    free(block);
}

void prog_block_set_stack_limit(prog_block_t * block)
{
    stmt_block_t *stmt;
    stmt = block->stmts;
    int limit = 0;
    while (stmt) {
        if (limit < stmt->stack_limit)
            limit = stmt->stack_limit;
        stmt = stmt->next;
    }
    block->stack_limit = limit;
}

void prog_block_set_locals_limit(prog_block_t * block, int limit)
{
    block->locals = limit; 
}

void prog_block_print(prog_block_t * block, FILE * output, const char * name)
{
    stmt_block_t *stmt;
    stmt = block->stmts;
    fprintf(output, jvm_const_invocation_fmt, name);

    fprintf(output, "%s", jvm_const_fun);
    fprintf(output, jvm_const_limit_stack_fmt, block->stack_limit);
    fprintf(output, jvm_const_limit_locals_fmt, block->locals);
    while (stmt) {
        stmt_block_print(stmt, output);
        stmt = stmt->next;
    }
    fprintf(output, "\t%s", jvm_const_return);
    fprintf(output, "%s\n\n", jvm_const_end_method);

    fprintf(output, "%s", jvm_const_main);
    fprintf(output, jvm_const_limit_stack_fmt, 0); // MAIN
    fprintf(output, jvm_const_limit_locals_fmt, 1);
    print_tab(output);
    fprintf(output, jvm_const_invokestatic_fun_fmt, name);
    fprintf(output, "\t%s", jvm_const_return);
    fprintf(output, "%s", jvm_const_end_method);

}

//=================== STATEMENTS BLOCKS
stmt_block_t * stmt_block_create()
{
    stmt_block_t *b;
    b = (stmt_block_t *) malloc(sizeof(stmt_block_t));
    b->next = NULL;
    b->expr = NULL;
    b->assign = -1; // Pure expression
    b->stack_limit = 0;
    return b;
}

void stmt_block_destroy(stmt_block_t * head)
{
    if (!head)
        return;
    expr_block_destroy(head->expr);
    stmt_block_destroy(head->next);
    free(head);
}

void stmt_block_build_assign(stmt_block_t * block, int loc, expr_block_t *expr)
{
    block->expr = expr;
    block->assign = loc;
    block->stack_limit = expr->stack_limit;
}

void stmt_block_build_expr(stmt_block_t * block, expr_block_t *expr)
{
    block->expr = expr;
    block->stack_limit = expr->stack_limit >= 2 ? expr->stack_limit : 2;
}

void stmt_block_print(stmt_block_t * block, FILE *output)
{
    if (block->assign == -1) {
        if (block->stack_limit > 1) { // Expression requires more than 1 stack element, use swap
            expr_block_print(block->expr, output); 
            print_tab(output);
            fprintf(output, "%s", jvm_const_getstatic_printstream);
            print_tab(output);
            fprintf(output, "%s", jvm_const_swap);
        } else { // Expression requires only one stack element, no need to use swap
            print_tab(output);
            fprintf(output, "%s", jvm_const_getstatic_printstream);
            expr_block_print(block->expr, output); 
        }
        print_tab(output);
        fprintf(output, "%s", jvm_const_invokevirtual_println);
    } else {
        expr_block_print(block->expr, output); 
        print_tab(output);
        if (0 <= block->assign && block->assign <= 3)
            fprintf(output, jvm_const_istore__fmt, block->assign);
        else
            fprintf(output, jvm_const_istore_fmt, block->assign);
    }
}

//=================== EXPRESSION BLOCKS

expr_block_t * expr_block_create()
{
    expr_block_t *b;
    b = (expr_block_t *) malloc(sizeof(expr_block_t));
    b->stack_limit = 0;
    b->fst = NULL;
    b->snd = NULL;
    b->content = NULL;
    b->use_swap = 0;
    return b;
}

void expr_block_destroy(expr_block_t * block)
{
    if (!block)
        return;
    expr_block_destroy(block->fst);
    expr_block_destroy(block->snd);
    if (block->content)
        free(block->content);
    free(block);
}

void expr_block_const(expr_block_t * block, int val)
{
    block->stack_limit = 1;
    block->fst = NULL;
    block->snd = NULL;
    block->content = (char *) malloc(sizeof(char) * 64);
    if (0 <= val && val < 6) // iconst_[0-6]
        sprintf(block->content, jvm_const_iconst_fmt, val);
    else if (val == -1) // iconst_m1
        sprintf(block->content, jvm_const_iconst_fmt, val);
    else if (-128<= val && val <= 127)
        sprintf(block->content, jvm_const_bipush_fmt, val);
    else if (-32768 <= val && val <= 32767)
        sprintf(block->content, jvm_const_sipush_fmt, val);
    else
        sprintf(block->content, jvm_const_ldc_fmt, val);
}

void expr_block_load(expr_block_t * block, int val)
{
    block->stack_limit = 1;
    block->fst = NULL;
    block->snd = NULL;
    block->content = (char *) malloc(sizeof(char) * 64);
    if (0 <= val && val < 4)
        sprintf(block->content, jvm_const_iload__fmt, val);
    else 
        sprintf(block->content, jvm_const_iload_fmt, val);
}

/** Generate binary operation code */
void expr_block_op(Exp _p_, expr_block_t *block)
{
    const char *const_str;
    switch(_p_->kind)
    {
        case is_ExpAdd:
            const_str = jvm_const_iadd;
            break;
        case is_ExpSub:
            const_str = jvm_const_isub;
            break;
        case is_ExpMul:
            const_str = jvm_const_imul;
            break;
        case is_ExpDiv:
            const_str = jvm_const_idiv;
            break;
        default:
            fprintf(stderr, "Wrong kind of expression in expr_block_op!\n");
            exit(0);
    }
    block->content = (char *) malloc(strlen(const_str)+1);
    strcpy(block->content, const_str);
}

void expr_block_build_op(expr_block_t *block_result, expr_block_t *block_a, 
        expr_block_t *block_b, int symmetric)
{
    block_result->use_swap = 0;
    if (block_a->stack_limit == block_b->stack_limit) {
        block_result->fst = block_a;
        block_result->snd = block_b;
        block_result->stack_limit = block_b->stack_limit + 1;
    } else if (block_a->stack_limit > block_b->stack_limit) {
        block_result->fst = block_a;
        block_result->snd = block_b;
        block_result->stack_limit = block_a->stack_limit;
    } else {
        block_result->fst = block_b;
        block_result->snd = block_a;
        block_result->stack_limit = block_b->stack_limit;
        if (!symmetric)
            block_result->use_swap = 1;
    }
    if (block_result->stack_limit < 2)
        block_result->stack_limit = 2;
}

void expr_block_print(expr_block_t * block, FILE *output)
{
    if (block->fst)
        expr_block_print(block->fst, output); 
    if (block->snd)
        expr_block_print(block->snd, output); 
    if (block->use_swap) {
        print_tab(output);
        fprintf(output, "%s", jvm_const_swap);
    }
    if (block->content){
        print_tab(output);
        fprintf(output, "%s", block->content);
    }
}

