OUT=out
SRC=src

GRAMMAR=${SRC}/Instant.cf
COMPILER=${SRC}/compiler
SCRIPTS=${SRC}/scripts

JVM_OUT=insc_jvm
LLVM_OUT=insc_llvm
ifeq ($(shell hostname), students)
	BNFC=/home/students/inf/PUBLIC/MRJP/bin/bnfc
else
	BNFC=bnfc

endif

all:
	rm -rf ${OUT}
	mkdir ${OUT}
	$(BNFC) -m -c -o ${OUT} ${GRAMMAR}
	cp ${COMPILER}/* ${OUT}
	$(MAKE) -C ${OUT}

	cp ${SCRIPTS}/insc_* .
	
clean:
	rm -rf $(OUT) insc_llvm insc_jvm tests/*.j tests/*.bc tests/*.ll tests/*.class

test-jvm: 
	./${SCRIPTS}/test.sh jvm
	rm -f tests/*.j tests/*.class

test-llvm: 
	./${SCRIPTS}/test.sh llvm
	rm -f tests/*.bc tests/*.ll


.PHONY: clean all tests-jvm tests-llvm
